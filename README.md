# religious-art-dataset


###  Directory Strucure
```
|-- dataset_splitted
  |-- test
    |-- other
    |-- religious
  |-- train
    |-- other
    |-- religious
  |-- validation
    |-- other
    |-- religious
```
### Image Resolution
* 512x512

### Image Count

* **Train Set**: 24329 (56.25%)
* **Validation Set**: 8110 (18.75%)
* **Test Set**: 10814 (25%)

* **Total Images**: 43253
* **Total Images Religious Class**: 22265
* **Total Images Other (Non-Religious) Class**: 20988

*** Dataset created for the "Trabajo Final de Master". José Fernando González M. jfgonzalezm@gmail.com ***


